package edu.uchicago.cs.gerber;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class NewTodoActivity extends Activity implements OnClickListener {

	private Button mBtnAdd;
	private EditText mEdtTitle, mEdtDetail;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.insert);
		mEdtTitle = (EditText) findViewById(R.id.edtTitle);
		mEdtDetail = (EditText) findViewById(R.id.edtDetail);

		mBtnAdd = (Button) findViewById(R.id.btnUpdate);
		mBtnAdd.setOnClickListener(this);

	}

	// ################################################
	// used for ActionBar
	// ################################################
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.actbar_insert, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			addTodo();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	//################################################
	// used for the button "add"
	//################################################
	@Override
	public void onClick(View v) {
		addTodo();

	}

	private void addTodo() {

		Todo tdoAdd = null;
		//Todo has a 2-arg construtor which leaves id as 0
		tdoAdd = new Todo(mEdtTitle.getText().toString(), mEdtDetail.getText()
				.toString());
		Intent itnReturn = new Intent();
		//add a serializable object (a Todo) to the intent
		itnReturn.putExtra(MainListActivity.TODO_KEY, tdoAdd);
		//set result to return from startActivityForResult (the itnReturn contains the Todo inside its bundle)
		setResult(MainListActivity.REQ_INSERT, itnReturn);
		//destroy this activity
		finish();

	}

}